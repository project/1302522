#!make
DEPENDENCIES=""
BRANCH_NAME=$(shell git rev-parse --abbrev-ref HEAD)
PWD=$(shell pwd)
include .env
export $(shell sed 's/=.*//' .env)

# Start container and build Drupal 7 locally
build-local-7:
	docker run --rm --name drupalci_${PROJECT_NAME} \
	    -v ${PWD}/:/var/www/html/modules/contrib/${PROJECT_NAME} \
	    -v ~/artifacts:/artifacts \
	    -p ${PROJECT_PORT}:80 \
	    -d marcelovani/drupalci:7-apache-interactive
	make build-local

build-local:
	docker exec -i drupalci_${PROJECT_NAME} bash -c "composer require ${DEPENDENCIES}"
	docker exec -i drupalci_${PROJECT_NAME} bash -c "sudo -u www-data drush si minimal --db-url=sqlite://sites/default/files/.ht.sqlite -y install_configure_form.update_status_module='array(FALSE,FALSE)'"
	docker exec -i drupalci_${PROJECT_NAME} bash -c "drush en -y simpletest"

# Test local build
test-local:
	docker exec -it drupalci_${PROJECT_NAME} bash -c '\
	    sudo -u www-data php scripts/run-tests.sh \
	    --php /usr/local/bin/php \
	    --verbose \
	    --color \
	    --concurrency "32" \
	    --url http://localhost \
	    --directory "modules/contrib/${PROJECT_NAME}"'

# Test in non-interactive mode
test-7:
	docker run --name drupalci_${PROJECT_NAME} \
	    -v ~/artifacts:/artifacts \
	    --rm marcelovani/drupalci:7-apache \
	    --project ${PROJECT_NAME} \
	    --version dev-1.x \
	    --dependencies ${DEPENDENCIES}

open:
	open "http://$(PROJECT_BASE_URL):${PROJECT_PORT}"

stop:
	docker stop drupalci_${PROJECT_NAME}

stop-all-containers:
	ids=$$(docker ps -a -q) && if [ "$${ids}" != "" ]; then docker stop $${ids}; fi

in:
	docker exec -it drupalci_${PROJECT_NAME} bash
